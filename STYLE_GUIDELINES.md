# C Style Guidelines

This document describes the style for writing C files. It is based on [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html).

## Table of Contents

1. [Introduction](#introduction)
2. [Naming](#naming)
3. [Comments](#comments)
3. [Formatting](#formatting)
    * [Line length](#line-length)
    * [Indentation](#indentation)
    * [Function declarations and definitions](#function-declarations-and-definitions)
    * [Function calls](#function-calls)
    * [Conditionals](#conditionals)
    * [Switch and loops statements](#switch-and-loops-statements)
    * [Pointer and reference expressions](#pointer-and-reference-expressions)

## Introduction

In progress...

## Naming

In progress...

## Comments

In progress...

## Formatting

### Line length

Each line of code should be at most 100 characters long. In punctual lines, you can increase the maximum size to 120 if that improves the readability considerably.

### Indentation

Use 4 spaces for indentation. Do not use tabs in the code.

### Function declarations and definitions

Return type on the same line as function name, parameters on the same line if they fit. Do not add spaces after the open paren or before the close paren.

Function declarations look like this:

```
return_type function_name(type_1 param_name_1, type_2 param_name_2) {
    ...
}  
```
 

If parameters do not all fit on one line, they should be broken up onto multiple lines, with each subsequent line aligned with the first parameter:
 
```
return_type function_name(type_1 param_name_1, type_2 param_name_2,
                         type_3, param_name_3) {
    ...
}
```

### Function calls

If it is possible, write the call all on a single line. Do not add spaces after the open paren or before the close paren.

Function calls have the following format:

```
return_type result = function_name(arg_name_1, arg_name_2);
```

If arguments do not all fit on one line, they should be broken up onto multiple lines, with each subsequent line aligned with the first argument:

```
return_type result = function_name(arg_name_1, arg_name_2,
                                   arg_name_3);
```

### Conditionals

Not include spaces between parenthesis and condition. The `else` goes on the same line as the closing brace.

```
if (condition_1) {
    ...
} else if (condition_2) {
    ...
} else {
    ...
}

```

### Switch and loops statements

Switch statements should use braces for blocks and always have a default case.

```
switch(var_name) {
    case 0: {
        ...
        break;
    }
    case 1: {
        ...
        break;
    }
    default: {
        ...
    }
}
```

Braces are also recommended for single-statement loops.

```
for(type var_name = init; var_name < finish; var_name = next) {
    ...
}
```

```
type var_name = init;
while (var_name < finish) {
    ...
    var_name = next;
}
```

### Pointer and reference expressions

When declaring a pointer variable or argument, you should place the asterisk adjacent to the variable name.

```
char *c;
const double &str;
```

No spaces around period or arrow. Pointer operators do not have trailing spaces.

```
x = *p;
p = &x;
x = r.y;
x = r->y;
```


