What is archetypal analysis?
============================

For a given dataset, archetypal analysis represents each individual as a mixture of individuals of pure type, called *archetypes*. At the same time, each archetype is a mixture of the dataset individuals.

Algorithm to find the archetypes
--------------------------------

Let :math:`x_1, \dots, x_n` be :math:`n` :math:`m`-dimensional data points. The problem is to find :math:`z_1, \dots, z_p` points where

.. math::
    z_k=\sum_{j=1}^n \beta_{kj}x_j

and :math:`z_1, \dots, z_p` minimize

.. math::
    RSS=\min_{\alpha_{ik}}\sum_{i=1}^n\left\Arrowvert x_i - \sum_{k=1}^p\alpha_{ik}z_k\right\Arrowvert^2

subject to the constraints

.. math::
    \alpha_{ik}\ge 0 \text{ for } i=1,\dots n; k=1,\dots,p \\
    \sum_{k=1}^p \alpha_{ik} = 1 \text{ for } i=1,\dots n \\
    \beta_{kj}\ge 0 \text{ for } k=1,\dots p; j=1,\dots,n \\
    \sum_{j=1}^n \beta_{kj} = 1 \text{ for } k=1,\dots p


The :math:`z_k`'s are the archetypes.
