Archetypal analysis
===================


.. toctree::
   :maxdepth: 1
   :caption: Introduction
   
   introduction

.. toctree::
   :maxdepth: 1
   :caption: Project API  
   
   api

.. toctree::
   :maxdepth: 1
   :caption: Useful information  
   
   license
