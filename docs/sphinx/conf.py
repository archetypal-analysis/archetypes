import subprocess

subprocess.call('cd ../doxygen && doxygen Doxyfile && cd ../sphinx', shell=True)

project = 'Archetypal Analysis'
copyright = '2019, A. Alcacer Sales'
author = 'A. Alcacer Sales'


extensions = ["sphinx.ext.mathjax", "breathe"]

source_suffix = '.rst'

master_doc = 'index'

language = None

exclude_patterns = ['.build', 'Thumbs.db', '.DS_Store']

pygments_style = None

html_theme = 'sphinx_rtd_theme'
html_logo = "logo-2.png"
html_show_sourcelink = False

html_theme_options = {
    "logo_only": True,
}

breathe_projects = { "include": "../doxygen/xml/" }
breathe_default_project = "include"
