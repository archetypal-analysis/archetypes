/*
 * Copyright (C) 2018  Aleix Alcacer
 */

/** @file archetypes.h
 * @brief Archetypes header file.
 *
 * This file contains Archetypes public API and the structures needed to use it.
 * @author A. Alcacer Sales <aleixalcacer@gmail.com>
 */

#ifndef ARCHETYPES_H
#define ARCHETYPES_H

#include <stdlib.h>

/**
 * @brief This function solves a penalized version of the non-negative least squares algorithm
 * (NNLS) described by Lawson and Hanson.
 *
 * In detail, the problems to solve are of the form
 * $$|Ax - b|_2$$ with \f$b\f$, \f$x\f$ vectors and \f$A\f$ a matrix, all of appropriate dimensions, and the
 * non-negativity and equality constraints.
 *
 * The penalized version adds an extra element to the
 * problem, then $$|Ax - b|_2 + M^2|cx - 1|_2$$ is minimized under
 * non-negativity restrictions being \f$M\f$ a constraint and \f$c\f$ a vector of ones.
 *
 * @param m Number of rows of matrix @p A
 * @param n Number of columns of matrix @p A
 * @param A Pointer to A data
 * @param b Pointer to b data (length @p n)
 * @param x Pointer where solution is stored (length @p m)
 *
 * @return An error code
 */

int lawson_hanson(int m, int n, double *A, double *b, double *x);

#endif //ARCHETYPES_ARCHETYPES_H
