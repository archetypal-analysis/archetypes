# C Basic Headers

This project is a collection of the basic headers used in the creation of C libraries.

By now, only macOS/Linux is supported.

## Table of Contents

1. [Usage](#usage)
2. [Modules](#modules)
    * [Assert](#assert)
    * [Memory](#memory)
    * [Test](#test)
    * [Time](#time)

## Usage

To use the features implemented in this collection only the next line should be added:

```
#include "c-basic-headers/cbh.h"
```

To see an example of use, see this [C project template](https://gitlab.com/c-basics/c-basic-project).

## Modules

In the following sections the modules functionality will be described.

### Assert

| Function | Definition |
|:---|:---:|
| `CBH_ASSERT_NULL(*a);` | Asserts if `a` pointer is `NULL`.|
| `CBH_ASSERT_NOT_NULL(*a);` | Asserts if `a` pointer is not `NULL`.|
| `CBH_ASSERT_EQUAL_INTEGER(a, b);` | For integers. Asserts if `a` is equal to `b`.|
| `CBH_ASSERT_EQUAL_FLOATING(a, b);` | For floatings. Asserts if `a` is equal to `b`.|
| `CBH_ASSERT_ALMOST_EQUAL_FLOATING(a, b, tol);` | For floatings. Asserts if `a` is equal to `b` with a `tol` tolerancy.|

### Memory

| Function | Definition |
|:---|:---:|
| `CBH_MEMORY_ALLOC(size);` | Allocates the requested memory and returns a pointer to it. |
| `CBH_MEMORY_CALLOC(nitems, size);` | Allocates the requested memory with zeros.|
| `CBH_MEMORY_REALLOC(ptr, size);` | attempts to resize the memory block pointed to by `ptr`.|
| `CBH_MEMORY_FREE(ptr);` |  Deallocates the memory previously allocated.|
| `CBH_MEMORY_SET(str, c, n);` | Copies the character `c` (an unsigned char) to the first `n` characters of the string pointed to by the argument `str`.|
| `CBH_MEMORY_CPY(str1, str2, n);` | copies `n` characters from memory area `str2` to memory area `str1`.|

### Test

This module contains an unit test that works as follows.

The first thing is decide if fixtures are desired or not.

If fixtures are not wanted only `CBH_TEST_TEST(test_name){}` is used. The next block code shows a simple example of it use:

```C
#include "../cbh.h"

CBH_TEST_TEST(example) {
    int a = 5;
    int b = 4;
    CBH_ASSERT_EQUAL_INTEGER(a, b);
    return 0;
}
```

However, if fixtures are wanted, doing a test is a bit more complex. 

The first thing to do is define all data parameters  needed in all fixtures. These parameters are stored in a struct called `data` using `CBH_TEST_DATA(test_name){};`. After that, setup data parameters is needed. To carry out that, `CBH_TEST_SETUP(test_name){}` is needed. 

When data is defined and initialized, each desired fixture is created using `CBH_TEST_FIXTURE(test_name, fixture_name){}`. Finally, to teardown all data parameters, the use of CBH_TEST_TEARDOWN(test_name`){}` is necessary. 

In the next block code there is a test example using fixtures:

```C
#include <stdlib.h>
#include "../cbh.h"

CBH_TEST_DATA(example2) {
    int *a;
}

CBH_TEST_SETUP(example2) {
    data->a = (int *) malloc(3 * sizeof(int))
    data->a[0] = 1;
    data->a[1] = 2;
    data->a[2] = 2;
}

CBH_TEST_FIXTURE(example2, fixture1) {
    CBH_ASSERT_EQUAL_INT(data->a[0], data->a[1]);
    return 0;
}

CBH_TEST_FIXTURE(example2, fixture2) {
    data->a[0] = 2;
    data->a[1] = 2;
    CBH_ASSERT_EQUAL_INT(data->a[1], data->a[2]);
    return 0;
}

CBH_TEST_TEARDOWN(example2) {
    free(data->a);
}
``` 

### Time

| Function | Definition |
|:---|:---:|
| `CBH_TIME_CURRENT(*time);` | Stores the current time into `time`.|
| `CBH_TIME_ELAPSED_NSEC(start, end);` | Returns the difference between `start` and `end` in nanoseconds.|
| `CBH_TIME_ELAPSED_SEC(start, end);` | Returns the difference between `start` and `end` in seconds.|
