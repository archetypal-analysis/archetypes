/*
 * Copyright (C) 2018  Aleix Alcacer
 */

#ifndef CBH_CBH_H
#define CBH_CBH_H

    #if defined __APPLE__
        #define CBH_OS_APPLE
    #endif

    #if defined __WIN32
        #define CBH_OS_WIN32
    #endif

    #if defined __linux__
        #define CBH_OS_LINUX
    #endif

    #include <time.h>
    #include <math.h>
    #include <stdio.h>
    #include <string.h>
    #include <stdlib.h>

    #include "modules/assert.h"
    #include "modules/test.h"
    #include "modules/time.h"
    #include "modules/memory.h"

#endif //CBH_CBH_H
