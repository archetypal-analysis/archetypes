/*
 * Copyright (C) 2018  Aleix Alcacer
 */

#include "../cbh.h"

#ifndef CBH_TEST_H
#define CBH_TEST_H

    typedef void (*cbh_test_setup_func)(void *);

    typedef void (*cbh_test_teardown_func)(void *);

    // Define the test struct
    struct cbh_test {
        const char *tname;  // suite name
        const char *fname;  // test name
        int (*run)();

        void *data;
        cbh_test_setup_func setup;
        cbh_test_teardown_func teardown;
        int skip;
        unsigned int magic;
    };

    //Define test name
    #define _CBH_TEST_NAME(name) cbh_test_##name

    // Define test struct name */
    #define _CBH_TEST_SNAME(test, fixture) _CBH_TEST_NAME(test##_##fixture)

    #define _CBH_TEST_MAGIC (0xdeadbccf)

    #if defined CBH_OS_WIN32
    #define CBH_TEST_IMPL_SECTION __declspec(allocate(".cbh_test$u")) __declspec(align(1))
    #endif

    #if defined CBH_OS_APPLE
    #define CBH_TEST_IMPL_SECTION __attribute__ ((used, section ("__DATA, .cbh_test"), aligned(1)))
    #endif

    #if defined CBH_OS_LINUX
    #define CBH_TEST_IMPL_SECTION __attribute__ ((used, section (".cbh_test"), aligned(1)))
    #endif

    // Define struct
    #define _CBH_TEST_STRUCT(test, fixture, tskip, tdata, tsetup, tteardown) \
        static struct cbh_test CBH_TEST_IMPL_SECTION _CBH_TEST_NAME(test##_##fixture) = { \
            .tname=#test, \
            .fname=#fixture, \
            .run = _CBH_TEST_NAME(test##_##fixture##_run), \
            .data = tdata, \
            .setup = (cbh_test_setup_func) tsetup, \
            .teardown = (cbh_test_teardown_func) tteardown, \
            .skip = tskip, \
            .magic = _CBH_TEST_MAGIC }

    #define CBH_TEST_DATA(test) struct test##_data

    #define CBH_TEST_SETUP(test) void test##_setup(struct test##_data *data)

    #define CBH_TEST_TEARDOWN(test) void test##_teardown(struct test##_data *data)


    #define _CBH_TEST_TEST(test, fixture, tskip) \
        static int _CBH_TEST_NAME(test##_##fixture##_run)(); \
        _CBH_TEST_STRUCT(test, fixture, tskip, NULL, NULL, NULL); \
        static int _CBH_TEST_NAME(test##_##fixture##_run)()

    #define _CBH_TEST_FIXTURE(test, fixture, tskip) \
        CBH_TEST_DATA(test) _CBH_TEST_NAME(test##_data); \
        CBH_TEST_SETUP(test); \
        CBH_TEST_TEARDOWN(test); \
        static int _CBH_TEST_NAME(test##_##fixture##_run)(struct  test##_data *data); \
        _CBH_TEST_STRUCT(test, fixture, tskip, &_CBH_TEST_NAME(test##_data), &test##_setup, &test##_teardown); \
        static int _CBH_TEST_NAME(test##_##fixture##_run)(struct test##_data* data)

    #define CBH_TEST_TEST(test, fixture) _CBH_TEST_TEST(test, fixture, 0)

    #define CBH_TEST_FIXTURE(test, fixture) _CBH_TEST_FIXTURE(test, fixture, 0)


    static const char *suite_name;
    typedef int (*cbh_test_filter_func)(struct cbh_test *);

    CBH_TEST_TEST(tname, fname) {return 0;}

    static int suite_all(struct cbh_test *t) {
        (void) t; // fix unused parameter warning
        return 1;
    }

    static int suite_filter(struct cbh_test *t) {
        return strncmp(suite_name, t->tname, strlen(suite_name)) == 0;
    }

    static int cbh_test_main(int argc, const char *argv[]) {
        static int total = 0;
        static int num_ok = 0;
        static int num_fail = 0;
        static int num_skip = 0;
        static int idx = 1;
        static cbh_test_filter_func filter = suite_all;

        if (argc == 2) {
            suite_name = argv[1];
            filter = suite_filter;
        }

        struct cbh_test *cbh_test_begin = &_CBH_TEST_SNAME(tname, fname);
        struct cbh_test *cbh_test_end = &_CBH_TEST_SNAME(tname, fname);
        // find begin and end of section by comparing magics
        while (1) {
            struct cbh_test *t = cbh_test_begin - 1;
            if (t->magic != _CBH_TEST_MAGIC) break;
            cbh_test_begin--;
        }

        cbh_test_begin += 2;

        while (1) {
            struct cbh_test *t = cbh_test_end + 1;
            if (t->magic != _CBH_TEST_MAGIC) break;
            cbh_test_end++;
        }
        cbh_test_end++;    // end after last one

        struct cbh_test *test;
        for (test = cbh_test_begin; test != cbh_test_end; test++) {
            if (test == &_CBH_TEST_SNAME(tname, fname)) continue;
            if (filter(test)) total++;
        }

        int err;

        for (test = cbh_test_begin; test != cbh_test_end; test++) {
            if (filter(test)) {
                if (test->data) {
                    printf("TEST %d/%d %s->%s ", idx, total, test->tname, test->fname);
                } else {
                    printf("TEST %d/%d %s ", idx, total, test->tname);
                }
                fflush(stdout);
                if (test->skip) {
                    printf("[SKIPPED]\n");
                    num_skip++;
                } else {
                    if (test->setup && *test->setup) {
                        (*test->setup)(test->data);
                    }
                    if (test->data) {
                        err = test->run(test->data);
                    } else {
                        err = test->run();
                    }
                    if (test->teardown && *test->teardown) {
                        (*test->teardown)(test->data);
                    }

                    if (err == 0) {
                        printf("[OK]\n");
                        num_ok++;
                    } else {
                        num_fail++;
                    }
                }
                idx++;
            }
        }
        printf("RESULTS: %d tests (%d ok, %d failed, %d skipped)\n", total, num_ok, num_fail, num_skip);
        return num_fail;
    }

#endif //CBH_TEST_H
