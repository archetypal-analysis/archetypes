/*
 * Copyright (C) 2018  Aleix Alcacer
 */

#include "../cbh.h"

#ifndef CBH_ASSERT_H
#define CBH_ASSERT_H

    #if defined CBH_OS_APPLE || CBH_OS_LINUX

        #define CBH_ASSERT(expr, msg) \
            if (expr) { } else { printf("[FAIL] %s %s:%d\n", msg, __FILE__, __LINE__); return 1; }

        #define CBH_ASSERT_NULL(a) \
            CBH_ASSERT(a == NULL, "pointer is not NULL")

        #define CBH_ASSERT_NOT_NULL(a) \
            CBH_ASSERT(a != NULL, "pointer is NULL")

        #define CBH_ASSERT_EQUAL_INTEGER(a, b) \
            CBH_ASSERT(a == b, "integers are not equal")

        #define CBH_ASSERT_EQUAL_FLOATING(a, b) \
            CBH_ASSERT(a == b, "floatings are not equal")

        #define CBH_ASSERT_ALMOST_EQUAL_FLOATING(a, b, tol) \
            CBH_ASSERT(fabs((a - b) / a) < tol, "floatings are not almost equal")

    #endif

#endif //CBH_ASSERT_H
