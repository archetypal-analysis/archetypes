/*
 * Copyright (C) 2018  Aleix Alcacer
 */

#include "../cbh.h"

#ifndef PROJECT_NAME_MEMORY_H
#define PROJECT_NAME_MEMORY_H

    #define CBH_MEMORY_ALLOC malloc
    #define CBH_MEMORY_CALLOC calloc
    #define CBH_MEMORY_REALLOC realloc
    #define CBH_MEMORY_FREE free
    #define CBH_MEMORY_SET memset
    #define CBH_MEMORY_CPY memcpy

#endif //PROJECT_NAME_MEMORY_H
