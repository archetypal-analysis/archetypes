/*
 * Copyright (C) 2018  Aleix Alcacer
 */

#include "../cbh.h"

    #ifndef CBH_TIME_H
    #define CBH_TIME_H

    #if defined CBH_OS_APPLE || CBH_OS_LINUX
        #define cbh_time_t struct timespec
        #define CBH_TIME_CURRENT(time) \
            cbh_time_current_unix(time)
        #define CBH_TIME_ELAPSED_NSEC(start, end) \
            cbh_time_elapsed_nsec_unix(start, end)
        #define CBH_TIME_ELAPSED_SEC(start, end)\
            cbh_time_elapsed_sec_unix(start, end)

        static int cbh_time_current_unix(cbh_time_t *time) {
            struct timespec tp;
            int result = clock_gettime(CLOCK_MONOTONIC, &tp);
            if (result != 0) {
                return result;
            }
            time->tv_sec = tp.tv_sec;
            time->tv_nsec = tp.tv_nsec;
            return 0;
        }

        static double cbh_time_elapsed_nsec_unix(cbh_time_t start, cbh_time_t end) {
            return (end.tv_sec * 1e9 + end.tv_nsec) - (start.tv_sec * 1e9 + start.tv_nsec);
        }

        static double cbh_time_elapsed_sec_unix(cbh_time_t start, cbh_time_t end) {
            return cbh_time_elapsed_nsec_unix(start, end) / 1e9;
        }
    #endif

#endif //CBH_TIME_H
