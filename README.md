# C Basic Project

Short description here.

## Project structure

```
├── CMakeLists.txt
├── LICENSE
├── STYLE_GUIDELINES.md
├── examples
│   └── CMakeLists.txt
├── include
│   └── c-basic-headers
│       ├── LICENSE.txt
│       ├── README.md
│       ├── cbh.h
│       └── modules
│           ├── assert.h
│           ├── test.h
│           └── time.h
├── src
└── tests
    └── CMakeLists.txt
```