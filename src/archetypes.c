/*
 * Copyright (C) 2018  Aleix Alcacer
 */

#include "archetypes/archetypes.h"
#include <stdbool.h>
#include <math.h>

int minv(int m, double *a) {
    return 0;
}

int mmm(bool *active_set, double *a, double *b, double *c) {
    return 0;
}

int mvm(bool *active_set, int m, int n, double* a, double *b, double *c) {
    return 0;
}

int vvm(bool *active_set, int m, double* a, double *b, double *c) {
    return 0;
}

int vvs(bool *active_set, int m, double* a, double *b, double *c) {
    return 0;
}

int compute_w(bool *active_set, int m, int n, int M, double *A, double *b, double *c, double *x,
              double *w) {
    return 0;
}

double max(const bool *active_set, int n, const double *a) {
    double a_max = -INFINITY;
    for (int i = 0; i < n; ++i) {
        if (active_set[i]) {
            a_max = (a[i] > a_max) ? a[i] : a_max;
        }
    }
    return a_max;
}

double min(const bool *active_set, int n, const double *a) {
    double a_min = INFINITY;
    for (int i = 0; i < n; ++i) {
        if (active_set[i]) {
            a_min = (a[i] < a_min) ? a[i] : a_min;
        }
    }
    return a_min;
}

int lawson_hanson(int m, int n, double *A, double *b, double *x) {

    int M = 200;
    int max_iter = 10000;
    int n_iter = 0;

    // Initialize c as a vector of ones
    double *c = (double *) malloc(n * sizeof(double));
    for (int i = 0; i < n; ++i) {
        c[i] = 1;
    }

    // Initialize x as a vector of 0
    for (int i = 0; i < n; ++i) {
        x[i] = 1;
    }

    // Initialize active_set as a bool vector
    bool *active_set = malloc(n * sizeof(bool));
    for (int i = 0; i < n; ++i) {
        active_set[i] = true;
    }

    // Calculate w
    double *w = (double *) malloc(n * sizeof(double));
    compute_w(active_set, m, n, M, A, b, c, x, w);

    // Compute if any element of active_set is true
    bool any = false;
    for (int i = 0; i < n; ++i) {
        any = any || active_set[i];
    }

    // Compute max of the w_i of the active_set
    double w_max = max(active_set, n, w);

    while (any && n_iter < max_iter && w_max > 1e-8) {

        //TODO: Implement code

        // Update w
        compute_w(active_set, m, n, M, A, b, c, x, w);

        // Compute if any element of active_set is true
        any = false;
        for (int i = 0; i < n; ++i) {
            any = any || active_set[i];
        }

        // Compute max of the w_i of the active_set
        w_max = max(active_set, n, w);

        // Update n_iter
        n_iter += 1;
    }
    return 0;
}